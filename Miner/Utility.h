#pragma once

class Utility{
public:
	static void drawRect(int x, int y, int w, int h, int color);
	static void drawRectBorder(int x, int y, int w, int h, int borderWidth, int color);
	static int getRandomInt(int lowerRange, int upperRange);
};

class ColorRGB
{
public:
	float r;
	float g;
	float b;

	enum Color{
		Black = 0,
		White = 1,
		Red = 2,
		Green = 3,
		Blue = 4,
		Yellow = 5,
		Purple = 6,
		Turquoise = 7,
		Brown = 8,
		TBD = 9
	};

	ColorRGB(Color _color){
		ColorRGB(static_cast<int>(_color));
	}

	ColorRGB(int colorID){
		if (colorID == 0){//black
			r = 0;
			g = 0;
			b = 0;
		}
		else if (colorID == 1){//white
			r = 1;
			g = 1;
			b = 1;
		}
		else if (colorID == 2){//red
			r = 1;
			g = 0;
			b = 0;
		}
		else if (colorID == 3){//greenn
			r = 0;
			g = 1;
			b = 0;
		}
		else if (colorID == 4){//blue
			r = 0;
			g = 0;
			b = 1;
		}
		else if (colorID == 5){//yellow?
			r = 1;
			g = 1;
			b = 0;
		}
		else if (colorID == 6){//purple?
			r = 1;
			g = 0;
			b = 1;
		}
		else if (colorID == 7){//turquoize?
			r = 0;
			g = 1;
			b = 1;
		}
		else if (colorID == 8){//?
			r = 0.5f;
			g = 0.2f;
			b = 0.1f;
		}
		else if (colorID == 9){//brown?
			r = 0;
			g = 1;
			b = 1;
		}
		else if (colorID == 10){//pink
			r = 0.5f;
			g = 0;
			b = 0;
		}
	}
};