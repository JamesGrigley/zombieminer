#include "stdafx.h"
#include "Player.h"
#include "Field.h"
#include "Utility.h"

Player::Player(float _width, float _height, float _xPos, float _yPos)
{
	xPos = _xPos;
	yPos = _yPos;
	width = _width;
	height = _height;
	lastGravityTick = GetTickCount();
	speed = 4.0f;
}
Player::Player()
{
}


Player::~Player()
{
}
void Player::move(int direction){
	//RIGHT
	if (direction == 2){
		xPos += speed;
	}
	//LEFT
	if (direction == 4){
		xPos -= speed;
	}

	//DOWN
	if (direction == 3)
		yPos -= speed;
}

void Player::start(){

}
void Player::update(int direction){
	if (GetTickCount() > lastGravityTick)
	{
		move(3);
		lastGravityTick = GetTickCount();
	}
	if (direction == 2 || direction == 4)
	{
		move(direction);
	}
}
void Player::draw(){
	Utility::drawRect(xPos, yPos, width, height, ColorRGB::Color::Red);
}