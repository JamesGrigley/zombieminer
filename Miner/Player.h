#pragma once
class Player
{
private:
	float lastGravityTick;

public:
	Player();
	Player(float _width, float _height, float _xPos, float _yPos);
	~Player();

	float xPos;
	float yPos;
	float height;
	float width;
	float speed;

	void move(int direction);

	void start();
	void update(int direction);
	void draw();
};

