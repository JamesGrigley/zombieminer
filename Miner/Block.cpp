#include "stdafx.h"
#include "Block.h"
#include "Field.h"
#include "Utility.h"

Block::Block()
{
}


Block::~Block()
{
}

Block::Block(int _x, int _y){
	xPos = _x;
	yPos = _y;
	blockType = BlockType::EmptyAir;
}

void Block::setType(BlockType _blockType){
	blockType = _blockType;
	if (blockType == BlockType::Dirt){
		color = ColorRGB::Color::Brown;
	}
}

void Block::draw(){
	Utility::drawRect(xPos, yPos, BLOCKWIDTH, BLOCKWIDTH, color);
	//Utility::drawRectBorder(xPos, yPos, BLOCKWIDTH, BLOCKWIDTH, 2, 0);
}
